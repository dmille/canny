% create a random 640x480 array of random numbers to test

test = randn(640,480);

sigma = 8;
dim = sigma*6+1;
gaus_1d_h = fspecial('gaussian', [1 dim], 8);
gaus_1d_v = fspecial('gaussian', [dim 1], 8);
gaus_2d = fspecial('gaussian', dim, 8);

tic();
out = imfilter(test, gaus_2d, 'conv');
toc();

tic();
out = imfilter(test, gaus_1d_h, 'conv');
out = imfilter(out, gaus_1d_v, 'conv');
toc();
