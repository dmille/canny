function out = canny( img, sigma, [lo hi] )
% takes a greyscale image, standard deviation, and magnitude threshold and
% returns a binary canny edge binary image

g = fspecial('gaussian', round(sigma*6)+1, sigma);

n = [1,-1];

img_norm = double(img);

dg_dx = imfilter(g, n, 'conv');
dg_dy = imfilter(g, n', 'conv');

img_dx = imfilter(img_norm, dg_dx, 'conv');
img_dy = imfilter(img_norm, dg_dy, 'conv');

% calculate img gradient and orientation
img_grad_mag = sqrt(img_dx.^2 + img_dy.^2);
img_grad_or = atan2(img_dx, img_dy);

% non-max suppression
[img_w, img_h] = size(img_norm);
img_supp = zeros(img_w, img_h);
tmp = padarray(img_grad_mag, [1 1]);
for i = 1:img_w
    for j = 1:img_h
        del_x = round(cos(img_grad_or(i,j)));
        del_y = round(sin(img_grad_or(i,j)));
        p = tmp(i+1,j+1);
        pp = tmp(i+del_x+1, j+del_y+1);
        pm = tmp(i-del_x+1, j-del_y+1);
        if((p > pp && p >= pm) || (p > pm && p >= pp))
            img_supp(i,j) = p;
        else
            img_supp(i,j) = 0;
        end
    end
end

% normalize the values in img_supp from 0.0 to 1.0
img_norm = mapminmax(img_supp,0,1.0);

% threshold
%img_thresh = img_norm > tau;

% apply hysteresis
for i = i:img_w
    for j = i:img_h
        if(img_norm > hi)
            % no useful data structures... really??
    end
end

% return resulting matrix
out = img_thresh;


end

