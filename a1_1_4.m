im = imread('lizard.jpg');
im_gray = double(rgb2gray(im));

h = fspecial('gaussian',13,2);

a = imfilter(im_gray, h, 'conv');
b = convo(im_gray, h);

fprintf('diff\n');
diff = abs(a)-abs(b);
imshow(diff);