im = imread('lizard.jpg');
im_gray = double(rgb2gray(im));

ker1 = [1,2,1; 2,4,2; 1,2,1]; % gaussian
ker2 = [1,2,1; 0,0,0; -1,-2,-1]; % horizantal sobel

gaus = convo(im_gray,ker1);
sobel = convo(im_gray,ker2);

fprintf('original\n');
imshow(im_gray, []);
pause();

fprintf('gaussian kernel\n');
imshow(gaus, []);
pause();

fprintf('sobel filter\n');
imshow(sobel, []);
