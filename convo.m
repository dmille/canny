function out = convo(img, ker)
    
    % get dimensions of the kernel and the image
    [ker_x, ker_y] = size(ker);
    [img_x, img_y] = size(img);
    
    % flip the kernel
    ker = flip(ker,1);
    ker = flip(ker,2);
    
    % pad the image with 0s
    pad_x = floor(ker_x/2);
    pad_y = floor(ker_y/2);
    
    tmp = padarray(img, [pad_x pad_y]);
   
    % preallocate array
    out = zeros(img_x,img_y);
    
    for i = 1:img_x
        for j = 1:img_y
            sum = 0;
            for k_i = 1:ker_x
                for k_j = 1:ker_y
                    sum = sum + ker(k_i,k_j) * tmp(i+k_i-1, j+k_j-1);
                end
            end
            out(i,j) = double(sum);
        end
    end
end